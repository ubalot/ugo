# Ugo (the umarell)

A [Umarell](https://en.wikipedia.org/wiki/Umarell) cli tool for people who are too lonely.

![Umarell mascot](/assets/images/umarell.jpg)

## Development

Run the app
```bash
racket main.rkt
```

Run the app with verbose output
```bash
racket main.rkt -v
```

Run the app with debug output
```bash
racket main.rkt -d
```


### Run tests

Run engine tests

```bash
raco test .
```

### Use different engine
Ugo (default)
```bash
racket main
```
or
```bash
racket main -e ugo
```

Eliza
```bash
racket main -e eliza
```

Markov chain
```bash
racket main -e markov
```

---
---

What can Ugo do??
Not so much for the moment, look at the screenshot for a preview
![Ugo the Umarell](/assets/images/ugo.png)
